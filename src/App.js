import React, { useState, useEffect } from 'react'
import Spinner from './Spinner'
import Images from './Images'
import Buttons from './Buttons'
import './App.css'
import S3FileUpload from 'react-s3';
import AWS from 'aws-sdk'
 
const s3_config = {
    bucketName: 'gesig-herkening',
    //: 'photos', /* optional */
    region: 'eu-west-1',
    accessKeyId: 'AKIASQPGP7ILHCHWH24Q',
    secretAccessKey: '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',
}

var S3_BUCKET_URL = 'https://gesig-herkening.s3-eu-west-1.amazonaws.com/'

const App = () => {
  const [uploading, setUploading] = useState(false);
  const [loadImages, setLoadImages] = useState(true);
  const [images, setImages] = useState([]);

  
  const onChange = e => {
    Array.from(e.target.files).forEach((file, i) => {
      console.log(file);
      S3FileUpload.uploadFile(file, s3_config)
        .then(data => {console.log(data); setUploading(false); retrieveImages()})
        .catch(err => {console.error(err); setUploading(false)});
    })
  };

  const retrieveImages = () => {
    setLoadImages(true);
    AWS.config.update({
      region: 'eu-west-1',
      accessKeyId: 'AKIASQPGP7ILHCHWH24Q',
      secretAccessKey: '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'
    });
  
    var s3 = new AWS.S3();
    var params = {Bucket : 'gesig-herkening'};
    var images = [];
    s3.listObjectsV2(params, (err, data) => {
      if (err) {
        console.log(err, err.stack);
        setImages([]);
      } else {
        //setListFiles(data.Contents);
        console.log(data.Contents);
        data.Contents.forEach(element => {
          images.push(S3_BUCKET_URL + element.Key)
        });
        setImages(images);
        setLoadImages(false);
      }
    });
  }

  const getImageContent = () => {
    if (loadImages || uploading) return <Spinner></Spinner>;
    return <Images images={images} />
  }

  useEffect(() => {
    retrieveImages();
  }, [setImages, setLoadImages]);

  return (
    <div>
      <div className='buttons'>
        <Buttons onChange={onChange} /> {getImageContent()}
      </div>
    </div>
  )
};
export default App;