import React from 'react'

export default function Images(props) { 
  return props.images.map((image, i) =>
    <div key={i} className='fadein'>
      <img src={image} alt='' />
    </div>
  )
}